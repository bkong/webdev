var ourData;
var row = document.getElementById('element');
var login = document.getElementById('login');
var user_photo = document.getElementById('user_photo');
var following = document.getElementById('followings');
var follower = document.getElementById('followers');
var subscription = document.getElementById('subscription');
var id = document.getElementById('id');
var index = 0;

$(document).ready(function(){
	$('#info').hide();
});

$(document).on('click', '.photo', function(){
	index = $(this).attr('id');
	$('#listFollowing').hide();
	$('#listFollower').hide();
	getCurrentUser();
	$('#user').hide();
	$('#info').show();

	
}); 


function request(){
	var ourRequest = new XMLHttpRequest();
	ourRequest.open('GET','https://api.github.com/users?since=135');
	ourRequest.onload = function(){
		ourData = JSON.parse(ourRequest.responseText);
		renderHTML(ourData);

	};
	ourRequest.send();
}

function renderHTML(data){
	for (var i = 0; i < data.length; i++) {
		var div = document.createElement('div');
		div.setAttribute('class','col-md-3');
		div.innerHTML = "<img src = "+"'"+ourData[i].avatar_url+"'"+" class='photo' id="+"'"+i+"'" +">";
		row.appendChild(div);
	}

}


function getCurrentUser(){
	var userData = ourData[index];
	user_photo.src= userData.avatar_url;
	id.innerHTML = userData.id;
	login.innerHTML = userData.login;
	getNumber(userData.followers_url, follower);
	getNumber(userData.following_url.slice(0,userData.following_url.length - 13), following);
	getNumber(userData.subscriptions_url, subscription);	

}

function getNumber(url,element){
	var length;
	var ourRequest = new XMLHttpRequest();
	ourRequest.open('GET',url);
	ourRequest.onload = function(){
		thisData = JSON.parse(ourRequest.responseText);
		length = thisData.length;
		element.innerHTML = length+"";

	};
	ourRequest.send();
}

function displayFollowers() {
	$("div").remove(".col-md-3");
	$('#listFollowing').hide();
	$('#listFollower').show();
	var userData = ourData[index];
	var thisData;
	var ourRequest = new XMLHttpRequest();
	ourRequest.open('GET',userData.followers_url);
	ourRequest.onload = function(){
		ourData = JSON.parse(ourRequest.responseText);
		list = document.getElementById('listFollower');
		for (var i = 0; i < ourData.length; i++) {
			var div = document.createElement('div');
			div.setAttribute('class','col-md-3');
			div.innerHTML = "<img src = "+"'"+ourData[i].avatar_url+"'"+" class='follower_photo photo' id="+"'"+i+"'" +">";
			list.appendChild(div);
		}

	};
	ourRequest.send();
}
function displayFollowing() {
	$("div").remove(".col-md-3");
	$('#listFollower').hide();
	$('#listFollowing').show();
	var userData = ourData[index];
	var thisData;
	var ourRequest = new XMLHttpRequest();
	ourRequest.open('GET',userData.following_url.slice(0,userData.following_url.length - 13));
	ourRequest.onload = function(){
		ourData = JSON.parse(ourRequest.responseText);
		list = document.getElementById('listFollowing');
		for (var i = 0; i < ourData.length; i++) {
			var div = document.createElement('div');
			div.setAttribute('class','col-md-3');
			div.innerHTML = "<img src = "+"'"+ourData[i].avatar_url+"'"+" class='following_photo photo' id="+"'"+i+"'" +">";
			list.appendChild(div);
		}

	};
	ourRequest.send();
}